from app import app
from flask import request, jsonify
from database import initDB, insertRole, db, toJson
from middleware import validateUser
from datetime import datetime
from config import config
import hashlib
import uuid
import re
import jwt



@app.route("/register", methods=["POST"])
def register():
    try:
        data = request.json

        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

        if re.search(regex, data["email"]):
            check_email = "select * from users where email = %s"
            _email = db.engine.execute(check_email, data["email"])
            row = [row for row in _email._metadata.keys]
            result_email = toJson(_email, row)
            db.session.close()

            user_id = str(uuid.uuid4())
            db_password = data["password"] + config["salt"]
            hash_password = hashlib.md5(db_password.encode()).hexdigest()

            if result_email == []:
                _verify_mail  = data["email"].split('@')

                if _verify_mail[1] == "sinw.com":
                    get_damin = "select * from roles where role_name = %s" 
                    _admin = db.engine.execute(get_damin, "admin")
                    row = [row for row in _admin._metadata.keys]
                    result_role_admin = toJson(_admin, row)
                    
                    if result_role_admin != []:
                        insert_client = """insert into users (user_id, email, password, role_id, create_at) values (%s, %s, %s, %s, %s)"""
                        db.engine.execute(insert_client, (user_id , data["email"], hash_password, result_role_admin[0]["role_id"], datetime.now()))
                        db.session.close()

                        insert_career = """insert into careers (career_id, user_id, career_name, salary, create_at) values (%s, %s, %s, %s, %s)"""
                        db.engine.execute(insert_career, (str(uuid.uuid4()) , user_id, data["career_name"], data["salary"], datetime.now()))
                        db.session.close()
                        return jsonify({"message": "register success", "status": "success", "data": ""}),200
                else:
                    get_user = "select * from roles where role_name = %s" 
                    _user = db.engine.execute(get_user, "nomal_user")
                    row = [row for row in _user._metadata.keys]
                    result_role_user = toJson(_user, row)

                    insert_client = """insert into users (user_id, email, password, role_id, create_at) values (%s, %s, %s, %s, %s)"""
                    db.engine.execute(insert_client, (user_id , data["email"], hash_password, result_role_user[0]["role_id"], datetime.now()))
                    db.session.close()

                    insert_career = """insert into careers (career_id, user_id, career_name, salary, create_at) values (%s, %s, %s, %s, %s)"""
                    db.engine.execute(insert_career, (str(uuid.uuid4()) , user_id, data["career_name"], data["salary"], datetime.now()))
                    db.session.close()
                    return jsonify({"message": "register success", "status": "success", "data": ""}),200
            else:
                return jsonify({"message": "dupicate email", "status": "fail", "data": ""}),200
        else:
            return jsonify({"message": "invalid format email", "status": "fail", "data": ""}),200
    except Exception as e:
        return jsonify({"message": str(e), "status": "fail", "data": ""}),400




@app.route("/login", methods=["POST"])
def login():
    try:
        ip_address = request.remote_addr
        data = request.json
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

        if re.search(regex, data["email"]):

            db_password = data["password"] + config["salt"]
            hash_password = hashlib.md5(db_password.encode()).hexdigest()

            check_user_login = """ select * from users where email = %s and password = %s"""
            _user_login = db.engine.execute(check_user_login, data["email"], hash_password)
            row = [row for row in _user_login._metadata.keys]
            result_login = toJson(_user_login, row)
            db.session.close()

            if result_login != []:

                payload_user = [{"user_id": _pac_data["user_id"], "role_id": _pac_data["role_id"]} for _pac_data in result_login]
                encoded_jwt = jwt.encode(payload_user[0], config["secret_key"], algorithm="HS256")

                log_history = """insert into history (history_id, user_id, email, ip_address, create_at) values(%s, %s, %s, %s, %s)"""
                db.engine.execute(log_history, (str(uuid.uuid4()), result_login[0]["user_id"], result_login[0]["email"], ip_address, datetime.now()))
                db.session.close()

                return jsonify({"message": "login success", "status": "success", "data": encoded_jwt}),200
            else:
                return jsonify({"message": "", "status": "fail", "data": ""}),200
        else:
            return jsonify({"message": "invalid format email", "status": "fail", "data": ""}),200
    except Exception as e:
        return jsonify({"message": str(e), "status": "fail", "data": ""}),400
    


@app.route('/add/career', methods=["POST"])
@validateUser
def addCareerAndSalary(warp):
    try:
        data = request.json
        user_id = request.args.get('user_id')

        if warp["role_name"] == "admin":
            insert_career = """insert into careers (career_id, user_id, career_name, salary, create_at) values(%s, %s, %s, %s, %s)"""
            db.engine.execute(insert_career, (str(uuid.uuid4()), user_id, data["career_name"], data["salary"], datetime.now()))
            db.session.close()
            return jsonify({"message": "insert career success role admin", "status": "success", "data": ""}),200
        else:
            if warp["user_id"] == user_id:
                insert_career = """insert into careers (career_id, user_id, career_name, salary, create_at) values(%s, %s, %s, %s, %s)"""
                db.engine.execute(insert_career, (str(uuid.uuid4()), user_id, data["career_name"], data["salary"], datetime.now()))
                db.session.close()
                return jsonify({"message": "insert career success role nomal_user", "status": "success", "data": ""}),200
            else:
                return jsonify({"message": f"cannot insert user invalid permission role: {warp['role_name']}", "status": "fail", "data": ""}),200
    except Exception as e:
        return jsonify({"message": str(e), "status": "fail", "data": ""}),400


@app.route('/edit/career', methods=["PUT"])
@validateUser
def editCareerAndSalary(warp):
    try:
        data = request.json
        user_id = request.args.get('user_id')

        if warp["role_name"] == "admin":
            update_career = """update careers set career_name=%s, salary=%s where career_id=%s"""
            db.engine.execute(update_career, (data["career_name"], data["salary"], data["career_id"]))
            db.session.close()
            return jsonify({"message": "update career success role admin", "status": "success", "data": ""}),200
        else:
            if warp["user_id"] == user_id:
                update_career = """update careers set career_name=%s, salary=%s where career_id=%s"""
                db.engine.execute(update_career, (data["career_name"], data["salary"], data["career_id"]))
                db.session.close()
                return jsonify({"message": "update career success role nomal_user", "status": "success", "data": ""}),200
            else:
                return jsonify({"message": f"cannot update user invalid permission role: {warp['role_name']}", "status": "fail", "data": ""}),200
    except Exception as e:
        return jsonify({"message": str(e), "status": "fail", "data": ""}),400

@app.route('/delete/account', methods=["DELETE"])
@validateUser
def deleteUser(warp):
    try:
        user_id = request.args.get('user_id')
        if warp["role_name"]=="nomal_user": 
            if warp["user_id"] == user_id:
                delete_account = """
                    delete from users where user_id=%s
                """
                db.engine.execute(delete_account, user_id)
                db.session.close()
                return jsonify({"message": "remove account member success", "status": "success", "data": ""}),200
            else:
                return jsonify({"message": f"cannot remove member, user is not owner", "status": "success", "data": ""}),200
        else:
            return jsonify({"message": f"cannot remove member, user invalid permission role: {warp['role_name']}", "status": "success", "data": ""}),200
    except Exception as e:
        return jsonify({"message": str(e), "status": "fail", "data": ""}),400


@app.route('/get/account', methods=["GET"])
@validateUser
def getUser(warp):
    try:
        if warp["role_name"] == "admin":
            get_all_account = """select * from users left join careers on users.user_id = careers.user_id"""
            get_all = db.engine.execute(get_all_account)
            row = [row for row in get_all._metadata.keys]
            result_get_all = toJson(get_all, row)
            db.session.close()
            return jsonify({"message": "get user success", "status": "success", "data": result_get_all}),200
        else:
            get_all_account = """select * from users left join careers on users.user_id = careers.user_id where role_id = %s"""
            get_all = db.engine.execute(get_all_account, warp["role_id"])
            row = [row for row in get_all._metadata.keys]
            result_get_all = toJson(get_all, row)
            db.session.close()
            return jsonify({"message": "get user success", "status": "success", "data": result_get_all}),200
    except Exception as e:
        return jsonify({"message": str(e), "status": "fail", "data": ""}),400


if __name__ == "__main__":
    initDB()
    insertRole()
    app.run(host="0.0.0.0", port=8008)