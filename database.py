from flask_sqlalchemy import SQLAlchemy
from app import app
import os
import uuid

DB_HOST = os.getenv('DB_HOST', default='127.0.0.1')
DB_PORT = os.getenv('DB_PORT', default='3333')
DB_USER = os.getenv('DB_USER', default='admin')
DB_PASSWORD = os.getenv('DB_PASSWORD', default='password')
DB_NAME = os.getenv('DB_NAME', default='siwattana')

url = "mysql+mysqlconnector://{}:{}@{}:{}/{}"\
    .format(DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME)
app.config["SQLALCHEMY_DATABASE_URI"] = url
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)


class Users(db.Model):
    __tablename__ = "users"
    user_id = db.Column(db.String(255), primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    role_id = db.Column(db.String(255), db.ForeignKey("roles.role_id", ondelete="cascade"))
    role_user = db.relationship("Roles", cascade="all,delete", backref='users', lazy=True)
    create_at = db.Column(db.DateTime)
    update_at = db.Column(db.DateTime)

class Roles(db.Model):
    __tablename__ = "roles"
    role_id = db.Column(db.String(255), primary_key=True)
    role_name = db.Column(db.String(255))
    
class History(db.Model):
    __tablename__="history"
    history_id = db.Column(db.String(255), primary_key=True)
    user_id = db.Column(db.String(255))
    email = db.Column(db.String(255))
    ip_address = db.Column(db.String(255))
    create_at = db.Column(db.DateTime)

class Careers(db.Model):
    __tablename__ = "careers"
    career_id = db.Column(db.String(255), primary_key=True)
    user_id = db.Column(db.String(255), db.ForeignKey("users.user_id", ondelete="cascade"))
    user_career = db.relationship("Users", cascade="all,delete", backref='carees', lazy=True)
    career_name = db.Column(db.String(255))
    salary = db.Column(db.String(255))
    create_at = db.Column(db.DateTime)


def initDB():
    # db.drop_all()
    db.create_all()

def toJson(data, columns):
    results = []
    for row in data:
        results.append(dict(zip(columns, row)))
    return results



def insertRole():
    get_admin = """select * from roles where role_name = %s"""
    check_admin = db.engine.execute(get_admin, "admin")
    row = [row for row in check_admin._metadata.keys]
    result_admin = toJson(check_admin, row)
    db.session.close()

    get_nomal_user = """select * from roles where role_name = %s"""
    check_nomal_user = db.engine.execute(get_nomal_user, "nomal_user")
    row = [row for row in check_nomal_user._metadata.keys]
    result_nomal_user = toJson(check_nomal_user, row)
    db.session.close()

    if result_admin == []:
        insert_admin = """insert into roles (role_id, role_name) 
                        values (%s, %s)"""
        db.engine.execute(insert_admin, (str(uuid.uuid4()), "admin"))
        db.session.close()

    if result_nomal_user == []:
        insert_nomal = """insert into roles (role_id, role_name) 
                        values (%s, %s)"""
        db.engine.execute(insert_nomal, (str(uuid.uuid4()), "nomal_user"))
        db.session.close()



