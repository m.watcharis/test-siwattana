import functools
from flask import request, jsonify
from database import db, toJson
from config import config
import jwt
import re


def validateUser(fn, *args, **kwds):
    @functools.wraps(fn)
    def warpper(*args, **kwds):

        get_access_token = request.headers.get('authorization')

        regex = "^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_=]*$"

        if get_access_token != "" and get_access_token != None and get_access_token != "Bearer" and get_access_token != "Bearer ":
            access_token = get_access_token.split(" ")[1]

            if re.search(regex, access_token):
                decode_jwt = jwt.decode(access_token, config["secret_key"], algorithms="HS256")

                check_user = """select * from users 
                            left join roles on users.role_id = roles.role_id
                            where users.user_id = %s and roles.role_id =%s
                        """
                _user = db.engine.execute(check_user, (decode_jwt["user_id"], decode_jwt["role_id"]))
                row = [row for row in _user._metadata.keys]
                result_login = toJson(_user, row)
                db.session.close()

                if result_login != []:
                    warp_data = {
                        "user_id": result_login[0]["user_id"],
                        "role_id": result_login[0]["role_id"],
                        "role_name": result_login[0]["role_name"]
                    }
                    return fn(warp_data)
            else:
                return jsonify({"message": "invalid format token", "status": "fail", "data": ""})
        else:
            return jsonify({"message": "Not found token", "status": "fail", "data": ""})
    return warpper